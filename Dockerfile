FROM debian:bullseye-slim

WORKDIR  /env

RUN for i in 1 2 3 4 5 6 7 8; do mkdir -p "/usr/share/man/man$i"; done && apt update && apt -y install python3 mkvtoolnix openjdk-11-jre-headless locales curl && \
    apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales procps &&\
        sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
            dpkg-reconfigure --frontend=noninteractive locales && \
                update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8 

COPY . /env

cmd ./fbchecker.sh


