# fbchecker-bash

A Dockerized Filebot Watcher

*  Supports watching multiple directories
*  Supports manually running a full scan over multiple directories

# Usage

Install `docker`, and optionally `docker-compose`.

Git clone this repository.

`# docker build`

There are several comments I've left in my script that will show you tweakable values, which you will more than likely have to tweak for your setup.

I've also included a sample docker-compose.yaml file that is optional, too. Which will likely need tweaking.

Feel free to edit the command string too, since filebot supports a multitude of formatting options for renaming.
