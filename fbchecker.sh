#!/bin/bash
bufArr=()
mediaArr=("/opt/mybook/public")
maxSubDelta=200
coolDown=300
while : 
do
	if ! ps aux | grep "[j]ava -Dapplication.deployment=tar"
	then 
		for i in ${!mediaArr[@]}
		do
			tempBuf=$(du -hs -B M ${mediaArr[$i]} | cut -f1 | sed 's/.$//')
			if [[ ! -z ${bufArr[$i]} ]]
			then
				if [[ $(($tempBuf-${bufArr[$i]})) -ge $maxSubDelta ]]
				then 
					echo "running check.." && ./newrename.sh
					unset bufArr
				fi
			fi
			bufArr[$i]=$tempBuf
		done
	fi
	sleep $coolDown
done

